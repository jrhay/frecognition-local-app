#!/usr/bin/env bash
sudo docker exec -it forslata_database_1 /bin/bash -c "ln -sf /usr/share/zoneinfo/CET /etc/localtime";
sudo docker exec -it forslata_pg_database_1 /bin/bash -c "ln -sf /usr/share/zoneinfo/CET /etc/localtime";
sudo docker exec -it forslata_video_module_1 /bin/bash -c "ln -sf /usr/share/zoneinfo/CET /etc/localtime";
#sudo docker exec -it fix_video_module_1 /bin/bash -c "ln -sf /usr/share/zoneinfo/CET /etc/localtime";
sudo docker exec -it forslata_server_1 /bin/bash -c "ln -sf /usr/share/zoneinfo/CET /etc/localtime";