#!/bin/bash
DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )"
BLACKLIST_DIR="$DIR/blacklist";

if [ -d "$BLACKLIST_DIR" ]; then
    mkdir -p "$BLACKLIST_DIR";
fi

if [ $? -ne 0 ]; then
    echo "ERROR: can't create blacklist directory in : $BLACKLIST_DIR, exiting...":
    exit 1;
fi

for file in `find $BLACKLIST_DIR -type f`
do
    echo "importing file:$file";
    basename=$(basename "$file");
    name=$(echo "$basename" | grep -o '[0-9]*-[0-9]*');
    year=${name:0:4};
    month=${name:4:2};
    day=${name:6:2};
    hour=${name:9:2};
    minute=${name:11:2};
    second=${name:13:2};
    microsecond=${name:15:6};
    new_name="${year}-${month}-${day}T${hour}-${minute}-${second}-${microsecond}";
    echo "basename is $name new name is $new_name";
    curl -F "name=$new_name" -F image[0]="@$file" 127.0.0.1:5000/auto_put/;
    break;
done
