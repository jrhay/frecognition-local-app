#!/usr/bin/env bash
DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )"
DB_FOLDER="$DIR/db";
BLACKLIST_DIR="$DIR/blacklist";
USER="gf";

echo "exporting OurFaces...";

#copy OurFaces directory from inside docker container
sudo docker cp docker1228_server_1:/app/static/OurFaces "$DB_FOLDER";
if [ $? -ne 0 ]; then
        echo "ERROR: while copying OurFaces directory";
        exit 1;
fi
zip -r "$DB_FOLDER/OurFaces.zip" "$DB_FOLDER/OurFaces";

echo "\ndone";



echo "exporting upload...";

#copy upload directory from inside docker container
sudo docker cp docker1228_server_1:/app/static/upload "$DB_FOLDER";
if [ $? -ne 0 ]; then
        echo "ERROR: while copying upload directory";
        exit 1;
fi
zip -r "$DB_FOLDER/upload.zip" "$DB_FOLDER/upload";

echo -e "done";

sudo chown -R "$USER:$USER" "$DB_FOLDER";

echo -e "\npermissions updated\n";
echo -e "===================>\n";
echo -e "\n\nOurFaces and upload folders are succesfully exported and put here : $DB_FOLDER\n\n";
ls -l "$DB_FOLDER";
echo -e "\n\n";