#!/bin/bash
DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )";
PULL_URL="127.0.0.1:5000/info";
TMP_DIR="$DIR/tmp";
RMUSER="gf";
PULL_RESULT_FILE="$TMP_DIR/info.json";
NEW_DATA_FILE="$DIR/new-data.json";
LOG_DIR="$DIR/logs";
ERROR_LOG="$LOG_DIR/error.log";
INFO_LOG="$LOG_DIR/info.log";

SERVER_URL="http://video-face-recognition.themarlab.com/data/recognition";

#Create directories
if [ ! -d "$TMP_DIR" ]; then
    mkdir "$TMP_DIR";
    chown "$RMUSER:$RMUSER" "$TMP_DIR";
fi

if [ ! -d "$LOG_DIR" ]; then
    mkdir "$LOG_DIR";
    chown "$RMUSER:$RMUSER" "$LOG_DIR";
fi

#Logging: create log files
if [ ! -f "$ERROR_LOG" ]; then
    touch "$ERROR_LOG";
    chown "$RMUSER:$RMUSER" "$ERROR_LOG";
fi

if [ ! -f "$ERROR_LOG" ]; then
    touch "$INFO_LOG";
    chown "$RMUSER:$RMUSER" "$INFO_LOG";
fi


#Logging: log functions

function get_date(){
    date=$(LANG=en_us_88591; date +"%Y-%m-%d %H:%M:%S");
    echo "$date";
}

function log() {
    if [ -z "$1" ]
    then
        echo "Error while log: empty log string";
        return;
    fi
    message="$1";
    echo "$message";

    date=$(get_date);
    echo "$date : $message" >> "$INFO_LOG";
}

function log_error() {
    if [ -z "$1" ]
    then
        echo "Error while log error: empty error log string";
        return
    fi
    message="$1";
    echo "$message";

    date=$(get_date);
    echo "$date : $message" >> "$ERROR_LOG";
}

function pull_results() {

    #pull to file
    curl "$PULL_URL" > "$PULL_RESULT_FILE";
    chown "$RMUSER:$RMUSER" "$PULL_RESULT_FILE";

    if [ $? -ne 0 ]
    then
        log_error "Error while get $PULL_URL results. Exit code: $?, response : $PULL_RESULT_FILE ";
        exit 1;
    fi
}

function update_server_data() {

    curl -X POST -H "Content-Type: application/json" -H "charset=utf-8" "$SERVER_URL" --data @- < "$NEW_DATA_FILE";

    if [ $? -ne 0 ]; then
        log_error "Error while put data to server";
        exit 1;
    else
        log "Data successfully sent to server";
    fi
}

##### Start #######

#pull results
pull_results;

#update server
update_server_data

##### END #########

log "successfully terminated";
exit 0;