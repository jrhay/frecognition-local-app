#!/usr/bin/env bash
DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )"
VIDEO_FOLDER="$DIR/../public/video";
VIDEO_FILE="$VIDEO_FOLDER/dashboard.m3u8";
PID_FILE="$DIR/../public/video/video.pid":
USER="gf:gf";

if [ ! -d "$VIDEO_FOLDER" ]; then
    mkdir -p "$VIDEO_FOLDER";
    chown -R "$USER" "$VIDEO_FOLDER";
fi

PID=$(cat "$PID_FILE");

if [ -f "$PID_FILE" ]; then
    echo "PID=$PID";
fi

if [ -n "$PID" ]; then
    kill "$PID";
fi

echo $$ > "$PID_FILE";

if [ ! -z "$(ls -A $VIDEO_FOLDER)" ]; then
   rm -r "$VIDEO_FOLDER/*";
fi

ffmpeg -v info -i rtsp://admin:admin@192.168.2.192 \
 -c:v copy -c:a copy -bufsize 1835k -pix_fmt yuv420p \
  -flags -global_header -hls_time 10 \
  -hls_list_size 6 -hls_wrap 10 -start_number 1 \
   "$VIDEO_FILE";