#!/bin/bash
DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )"
PULL_FILE="$DIR/tmp/info.json";
JOBS_DIR="$DIR/tmp/img/job";
BLACKLIST_DIR="$DIR/blacklist";
IMAGE_DIR="$DIR/../public/img";
RMUSER="gf";

#create directories if necessary
if [ ! -d "$DIR/tmp" ]; then
    mkdir "$DIR/tmp";
    chown "$RMUSER:$RMUSER" "$DIR/tmp";
fi

if [ ! -d "$DIR/tmp/img" ]; then
    mkdir "$DIR/tmp/img";
    chown "$RMUSER:$RMUSER" "$DIR/tmp/img";
fi

if [ ! -d "$DIR/tmp/img/job" ]; then
    mkdir "$DIR/tmp/img/job";
    chown "$RMUSER:$RMUSER" "$DIR/tmp/img/job";
fi

if [ ! -d "$DIR/tmp/img/sent" ]; then
    mkdir "$DIR/tmp/img/sent";
    chown "$RMUSER:$RMUSER" "$DIR/tmp/img/sent";
fi

get_new_name(){
    image="$1";
    image=${image/\"/};
    year=${image:0:4};
    month=${image:5:2};
    day=${image:8:2};
    hour=${image:11:2};
    minute=${image:14:2};
    second=${image:17:2};
    micro=${image:20:6};
    echo "${year}${month}${day}-${hour}${minute}${second}${micro}";
}

# get main images of every person

images=$(jq -c  '.[0] | values[] | .Name ' "$PULL_FILE");
for image in ${images}
do


    image=${image/\"/};
    newName=$(get_new_name "$image");

    if [ -f "$BLACKLIST_DIR/$newName.jpg" ]; then
           IMAGE_URL="127.0.0.1:5000/static/OurFaces";
           image_url="${IMAGE_URL}/${image}.jpg";
    else
         IMAGE_URL="127.0.0.1:5000/static/upload/Unrecognized";
         image_url="${IMAGE_URL}/${image}/${image}.jpg";
    fi

    filename="${JOBS_DIR}/${newName}.jpg";
    imgFilename="${IMAGE_DIR}/${newName}.jpg";
    if [ ! -f "$filename" ]; then
            echo "image url is $image_url";
            curl "$image_url" > "$filename";
            chown "$RMUSER:$RMUSER" "$filename";
    else
            echo "job exists";
    fi

    if [ ! -f "$imgFilename" ]; then
            cp "$filename" "$imgFilename";
    fi

    if [ ! -f "$imgFilename" ]; then
	    cp "$filename" "$imgFilename";
    fi
done


#get last visit image of every person from blacklist
IMAGE_URL="127.0.0.1:5000/static/upload";
images=$(jq -c  '.[0] | values[] | .Name ' "$PULL_FILE");
for image in ${images}
do
    image=${image//\"/};
    newName=$(get_new_name "$image");

    #image=$(echo "$image" | grep -o  '[0-9]*-[0-9]*');
    #check if in blacklist directory

    if [ ! -f "$BLACKLIST_DIR/$newName.jpg" ]; then
        continue;
    else
        echo "person $image is in blacklist, process...";
    fi

    last_visit=$(jq -c ".[0] | values[] | select(.Name==\"$image\") | .LastCached[-1]" "$PULL_FILE");

    #last_visit=$(echo "$last_visit" | grep -o  '[0-9]*-[0-9]*');

    if [ -z "$last_visit" ]; then
        echo "no last visit for $image";
        continue;
    fi

    last_visit_image=$(echo "$last_visit" | cut -d'/' -f 5);
    last_visit_image=${last_visit_image//\"/};

    if [ -z "$last_visit_image" ]; then
	echo "no last visit image";
	continue;
    fi

    filename="${JOBS_DIR}/$newName-blacklist.jpg";
    imgFilename="${IMAGE_DIR}/$newName.jpg";

#    if [ ! -f "$filename" ]; then
            image_url="${IMAGE_URL}/${image}/${last_visit_image}";
            echo "image url is $image_url,last_visit is $last_visit,  last_visit_image is $last_visit_image";
            curl "$image_url" > "$filename";
            chown "$RMUSER:$RMUSER" "$filename";
#    else
#            echo "$image_url job exists";
#    fi

    localFilename="${IMAGE_DIR}/${newName}-blacklist.jpg";
#    if [ -f "$filename" ] && [ ! -f "$localFilename" ]; then
        cp "$filename" "$localFilename";
        echo "\n filename=$filename";
        echo "\n localFilename=$localFilename";
#    fi
done