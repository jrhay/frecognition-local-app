#!/bin/bash
DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )"
DB_FOLDER="$DIR/db";
BLACKLIST_DIR="$DIR/blacklist";
USER="gf";

#create folder
if [ ! -d "db" ]; then
    mkdir -p "$DB_FOLDER";
    if [ $? -ne 0 ]; then
        echo "ERROR: can't create $DB_FOLDER directory. Exiting..";
        exit 1;
    fi
fi

#check if docker is running
running_containers=$(sudo docker ps -q);
is_running='true'
if [ -z "$running_containers" ]; then
    is_running="false";
    echo "WARNING: no containers are running";
else
    is_running=$(sudo docker inspect -f "{{.State.Running}}" "forslata_database_1");

    if [ "$is_running" = "false" ]; then
        is_running=$(sudo docker inspect -f "{{.State.Running}}" "forslata_video_module_1");
    fi

    if [ "$is_running" = "false" ]; then
        is_running=$(sudo docker inspect -f "{{.State.Running}}" "forslata_server_1");
    fi
fi

#exit if some problems
if [ $? -ne 0 ]; then
    echo "ERROR: exiting...";
    exit 1;
fi

cd /home/gf/for_slata;

if [ "$is_running" = "false" ]; then
	echo "WARNING: docker is not running";

else
 	sudo docker-compose down;
	echo "docker is down";
fi

sudo docker-compose up -d;

#set correct timezones
echo "setting correct timezones..."
bash "$DIR/set-container-timezones.sh";
echo "done";

#load blacklist
if [ -d "$BLACKLIST_DIR" ]; then
    echo "loading blacklist...";
    bash "$DIR/load-blacklist.sh";
    echo "done";
fi



echo "done";