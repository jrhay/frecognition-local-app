#!/bin/bash
for directory in `find /home/retail-metrika/retail-metrika/image_base -maxdepth 4 -mindepth 4 -type d`
do
	for file in `find "$directory" -type f`
	do
		echo "importing file:$file";
		name=$(date '+%Y-%m-%d-%H:%M:%S-%N');
		curl -F "name=$name" -F image[0]="@$file" 127.0.0.1:5000/auto_put/;
		break;
	done
done
