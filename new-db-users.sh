#!/bin/bash

PULL_URL="127.0.0.1/pull";
TMP_DIR="$PWD/tmp";
PULL_RESULT_FILE="$TMP_DIR/pull.json";

LAST_UNRECOGNIZED_RECORD_FILE="$PWD/last_handled_record";

LOG_DIR="$PWD/logs";
ERROR_LOG="$LOG_DIR/error.log";
INFO_LOG="$LOG_DIR/info.log";

SERVER_URL="http://video-face-recognition.themarlab.com.com/adamas/update";

#Create directories
if [ ! -d "$TMP_DIR" ]; then
    mkdir "$TMP_DIR";
fi

if [ ! -d "$LOG_DIR" ]; then
    mkdir "$LOG_DIR";
fi

#Logging: create log files
if [ ! -f "$ERROR_LOG" ]; then
    touch "$ERROR_LOG";
fi

if [ ! -f "$ERROR_LOG" ]; then
    touch "$INFO_LOG";
fi


#Logging: log functions

function get_date(){
    date=$(LANG=en_us_88591; date +"%Y-%m-%d %H:%M:%S");
    echo "$date";
}

function log() {
    if [ -z "$1" ]
    then
        echo "Error while log: empty log string";
        return;
    fi
    message="$1";
    echo "$message";

    date=$(get_date);
    echo "$date : $message" >> "$INFO_LOG";
}

function log_error() {
    if [ -z "$1" ]
    then
        echo "Error while log error: empty error log string";
        return
    fi
    message="$1";
    echo "$message";

    date=$(get_date);
    echo "$date : $message" >> "$ERROR_LOG";
}

#get last unrecognized record
function get_last_unrecognized_record() {
    last_unrecognized="";

    if [ -f "$LAST_UNRECOGNIZED_RECORD_FILE" ]; then
       last_unrecognized=$(cat "$LAST_UNRECOGNIZED_RECORD_FILE"); 
    fi

    echo "$last_unrecognized";
}

function set_last_unrecognized_record() {
    log "set $1 as last_unrecognized_record";
    if [ -z $1 ]; then
        log_error "Error while set l.u.r. value : Empty last unrecognized record string value" ;
        exit 1;
    fi

    if [ -f "$LAST_UNRECOGNIZED_RECORD_FILE" ]; then
        touch "$LAST_UNRECOGNIZED_RECORD_FILE";
    fi

    echo "$1" > "$LAST_UNRECOGNIZED_RECORD_FILE";
}

#get data

function pull_results() {
    response=$(curl "127.0.0.1:5000/pull/");
    if [ $? -ne 0 ]
    then
        log_error "Error while get 127.0.0.1:5000/pull/ results. Exit code: $?, response : $response ";
        if [ -f "$PULL_RESULT_FILE" ]; then
            echo "" >  "$PULL_RESULT_FILE";
        fi

        exit 1;
    fi

    echo $response > "$PULL_RESULT_FILE";
}

#return unrecognized string array
function get_time_records() {
    #get new data 
    pull_results;

    #get Time records;
    time_records=$(jq -c '.[] | select(.Name | contains("Unrecognized")) | .Time | .[]' "$PULL_RESULT_FILE");
    
    echo "$time_records";
}

#add image to db
function handle_record() {

    log "Start handling record $1";

    if [ -z $1 ]; then
        log_error "Can't handle empty record";
        exit 1;
    fi

    record="$1";

    image_url="http://127.0.0.1:5000/static/upload/Unrecognized/${record}.jpg";

    echo "image url: $image_url";
    
    image_file="$TMP_DIR/$record.jpg";
    
    download_result=$(curl "$image_url" > "$image_file");
    
    if [ $? -ne 0 ]; then
        log_error "Error while download image $image_url";
        exit 1;
    fi

    echo "Insert into db $record, Upload image: $image_file";

    put_result=$(curl -F "name=$record" -F "image[0]=@$image_file" "http://127.0.0.1:5000/put/");

    if [ $? -ne 0 ]; then
        log_error "Error while put unrecognized image ot database : $putResult";
        exit 1;
    else
        log "New record $record with image url $imageUrl successfully added to database";
        return 0;
    fi
}

# put data to our server, takes $1 parameter as json string of data

function update_server_data() {

    data="$1";
    if [ -z "$data" ]; then
        log_error "Error while put data to server: empty string";
        exit 1;
    fi
    
    validate_json=$(echo '$data' | jq);

    if [ $? -ne 0 ]; then
        log_error "Error while put data to server: bad json string : $data";
        exit 1;
    fi

    $response=$(curl -X POST -H "Content-Type: application/json" -H "charset=utf-8" "$SERVER_URL" --data "$data");

    if [ $? -ne 0 ]; then
        log_error "Error while put data to server: bad response: $response";
        exit 1;
    else
        log "Data successfully sent to server";
    fi
}

##### Start working #######

#pull results

time_records=$(get_time_records);
last_unrecognized_record=$(get_last_unrecognized_record);

count=1;
handle_record=0;
for record in $time_records
do  
    record=$(echo "$record" | grep -o  '[0-9]*-[0-9]*') ;
    #if bad record continue
    if [ -z "$record" ]; then
        continue;
    fi
    echo "loop record $record";
    #if first execute will run from first record
    if [ -z "$last_unrecognized_record" ]; then
        handle_record=1;
    fi

    #handle
    if [ "$handle_record" -eq 1 ]; then
        handle_record $record;
        
        #if record successfully handled update last handled record otherwise exit with error
        if [ $? -eq 0 ]; then
            set_last_unrecognized_record $record;
        else
            log_error "Error while handle record";
            exit 1;
        fi
    fi

    #check if record already handled and set handle_record=1 to handle this record and all records after it.
    if [ -n "$last_unrecognized_record" ] && [ "$record" == "$last_unrecognized_record" ]; then
        echo "setting \$handl_record=1";
        handle_record=1;
    fi
    
        
    #update counter
    let "count+=1";
    
    #debug:
    #if [ $count -gt 10 ]; then
    #    exit 0;
    #fi
done

#put data to server
data=$(cat "$PULL_RESULT_FILE");
if [ -n "$data" ]; then
    update_server_data "$data";
else
    log_error "Error while post data to server: empty data string;"
fi

log "successfully terminated";
exit 0;
