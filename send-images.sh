#!/bin/bash
DIR="$( cd "$( dirname "$0" )" >/dev/null && pwd )"
JOBS_DIR="$DIR/tmp/img/job";
SEND_DIR="$DIR/tmp/img/sent";
SERVER_URL="video-face-recognition.themarlab.com/data/image";

count=0

for image in $JOBS_DIR/* ; do

	image_basename=$(basename "$image");
	sent_image_url="$SEND_DIR/$image_basename";

	if [ -f "$image" ] && [ ! -f "$sent_image_url" ]; then
		sleep 3;
		curl -F  "image=$image" -F "image=@$image" "$SERVER_URL"
		if [ $? -eq 0 ]; then
			touch "$sent_image_url";
			let "count+=1";
		fi

		if [ $count -ge 15 ]; then
			break;
		fi
	else
		echo "file is already sent";
	fi
done
	
exit 0;
