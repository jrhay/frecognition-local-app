is_running=$(docker inspect -f "{{.State.Running}}" "forslata_database_1");

if [ "$is_running" = "false" ]; then
	is_running=$(docker inspect -f "{{.State.Running}}" "forslata_video_module_1");
fi

if [ "$is_running" = "false" ]; then
	is_running=$(docker inspect -f "{{.State.Running}}" "forslata_server_1");
fi

if [ "$is_running" = "false" ]; then
	echo "start retail-metrika docker";
	cd /home/gf/for_slata;
 	docker-compose up;
else
	echo "retail-metrika docker is already running";
fi
